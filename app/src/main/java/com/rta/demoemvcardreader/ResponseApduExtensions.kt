package com.rta.demoemvcardreader

import net.sf.scuba.smartcards.ResponseAPDU
import net.sf.scuba.util.Hex
import timber.log.Timber

fun ResponseAPDU.isSuccess():  Boolean {
    Timber.d("Response SW: ${Hex.intToHexString(sw)}")
    return sw == 0x90_00
}