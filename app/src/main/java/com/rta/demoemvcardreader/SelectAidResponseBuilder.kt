package com.rta.demoemvcardreader

import com.rta.demoemvcardreader.SelectAidResponse

class SelectAidResponseBuilder {
    var dfName: ByteArray = byteArrayOf()
    var pDol: ByteArray = byteArrayOf()
    var logEntry: ByteArray = byteArrayOf()
    var appPreferredName: String = ""
    var appPriorityIndicator: String = ""
    var languagePreference: String = ""
    var issuerCodeTableIndex: String = ""

    fun build(): SelectAidResponse = SelectAidResponse(
        dfName,
        pDol,
        logEntry,
        appPreferredName,
        appPriorityIndicator,
        languagePreference,
        issuerCodeTableIndex
    )
}