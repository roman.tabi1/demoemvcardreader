package com.rta.demoemvcardreader

import net.sf.scuba.smartcards.CommandAPDU

object ApduCommands {

    fun select(data: ByteArray): CommandAPDU =
        CommandAPDU(0x00, 0xA4, 0x04, 0x00, data)

//    80A80000 15 83 13F620C000000000000001528ADF67020320030900
    fun getProcessingOptions(data: ByteArray): CommandAPDU =
        CommandAPDU(0x80, 0xA8, 0x00, 0x00, data)

    fun get(p0: Byte, p1: Byte): CommandAPDU =
        CommandAPDU(0x80, 0xCA, p0.toInt(), p1.toInt())

    fun readSfi(p0: Int, p1: Int): CommandAPDU =
        CommandAPDU(0x00, 0xB2, p0, p1)
}