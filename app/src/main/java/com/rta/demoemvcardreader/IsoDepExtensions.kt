package com.rta.demoemvcardreader

import android.nfc.tech.IsoDep
import net.sf.scuba.smartcards.CommandAPDU
import net.sf.scuba.smartcards.ResponseAPDU
import net.sf.scuba.util.Hex
import timber.log.Timber

fun IsoDep.transceive(commandAPDU: CommandAPDU): ResponseAPDU {
    val bytes = commandAPDU.bytes
    val fullBytes = ByteArray(bytes.size + 1) { 0x00 }.apply {
        bytes.copyInto(this)
    }
    Timber.d("-- Apdu: ${Hex.bytesToSpacedHexString(fullBytes)}")

    val responseAPDU = ResponseAPDU(this.transceive(fullBytes))
    Timber.d("-- Response: ${Hex.bytesToSpacedHexString(responseAPDU.bytes)}")

    return responseAPDU
}