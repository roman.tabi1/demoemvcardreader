package com.rta.demoemvcardreader

const val APP_LABEL = 0x50
const val AID = 0x4F
const val APP_TEMPLATE = 0x61
const val FCI = 0xBF_0C
const val DF_NAME = 0x84
const val PDOL = 0x9F_38
const val LOG_FORMAT = 0x9F_4F
const val LOG_ENTRY = 0x9F_4D
const val APPLICATION_PREFERRED_NAME= 0x9F12
const val API= 0x87
const val LANG_PREFERENCE = 0x5F2D
const val ISSUER_CODE_TABLE_INDEX = 0x9F11
const val PAN = 0x5A
const val EXPIRATION = 0x5F24
