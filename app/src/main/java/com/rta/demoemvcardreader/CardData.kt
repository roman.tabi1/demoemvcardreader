package com.rta.demoemvcardreader

data class CardData(val pan: String, val expiration: String)