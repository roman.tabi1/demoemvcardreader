package com.rta.demoemvcardreader

import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import net.sf.scuba.util.Hex
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    private var nfcAdapter: NfcAdapter? = null
    private var scope: CoroutineScope? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Timber.plant(Timber.DebugTree())

        nfcAdapter = NfcAdapter.getDefaultAdapter(application)

        startDiscovery()
    }

    private fun startDiscovery() {
        nfcAdapter?.let { adapter ->
            if (adapter.isEnabled) {
                adapter.enableReaderMode(
                    this,
                    ::handleTag,
                    NfcAdapter.FLAG_READER_NFC_A,
                    null
                )
            } else {
                Timber.e("-- NFC disabled")
            }
        } ?: Timber.e("-- NFC adapter nod found")
    }

    private fun handleTag(tag: Tag) {
        Timber.d("-- New tag received: $tag")
        scope?.cancel()
        CoroutineScope(Dispatchers.IO).launch {
            val isoDep = IsoDep.get(tag).also { it.connect() }

            val appTemplates = getApplicationIds(isoDep)
            appTemplates?.forEach {
                val selectAidResponse = selectAid(isoDep, it) ?: return@launch
                val gpoResponse = getProcessingOptions(isoDep, selectAidResponse) ?: return@launch

                if (gpoResponse.track2.isNotEmpty()) {
                    // TODO: parse track2 data and get PAN and Expiration
                    Timber.d("-- Card data: ${gpoResponse.track2}")
                } else {
                    val card =
                        readCardData(isoDep, gpoResponse.applicationFileLocator) ?: return@launch
                    Timber.d("-- Card data: $card")
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        scope?.cancel()
    }
}