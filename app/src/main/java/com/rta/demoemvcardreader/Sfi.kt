package com.rta.demoemvcardreader

data class Sfi(val number: Int, val record: Int)