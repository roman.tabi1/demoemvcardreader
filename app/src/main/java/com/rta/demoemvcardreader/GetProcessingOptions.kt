package com.rta.demoemvcardreader

import android.nfc.TagLostException
import android.nfc.tech.IsoDep
import net.sf.scuba.tlv.TLVInputStream
import net.sf.scuba.util.Hex
import timber.log.Timber
import java.security.SecureRandom
import java.text.SimpleDateFormat
import java.util.*

fun getProcessingOptions(isoDep: IsoDep, selectAidResponse: SelectAidResponse): GpoResponse? {

    Timber.d("-- Trying to Get Processing Options")

    val gpoCommandData = if (selectAidResponse.pDOL.isNotEmpty()) {
        getGpoCommandDataFromPdol(selectAidResponse.pDOL)
    } else {
        Hex.hexStringToBytes("8300")
    }

    var gpoResponse: GpoResponse? = null
    try {
        val responseAPDU =
            isoDep.transceive(ApduCommands.getProcessingOptions(gpoCommandData)).also {
                if (!it.isSuccess()) {
                    return null
                }
            }

        TLVInputStream(responseAPDU.bytes.inputStream()).use { stream ->
            val tag = stream.readTag()
            stream.readLength()

            gpoResponse = when (tag) {
                0x77 -> parseTlvGpoResponse(stream.readValue())
                0x80 -> parseNonTlvGpoResponse(stream.readValue())
                else -> throw Exception("Unknown GPO response template: ${Hex.intToHexString(tag)}")
            }
        }
    } catch (exception: TagLostException) {
        return null
    } catch (exception: Exception) {
        Timber.e(exception)
    }

    Timber.d("$gpoResponse")
    return gpoResponse
}

private fun parseTlvGpoResponse(data: ByteArray): GpoResponse {
    Timber.e("parseTlvGpoResponse")
    TLVInputStream(data.inputStream()).use { stream ->
        val afl = try {
            stream.skipToTag(0x94)
            stream.readLength()
            stream.readValue()
        } catch (exception: Exception) {
            byteArrayOf()
        }
        val track2 = try {
            stream.skipToTag(0x57)
            stream.readLength()
            stream.readValue()
        } catch (exception: Exception) {
            byteArrayOf()
        }
        return GpoResponse(byteArrayOf(), afl, track2)
    }
}

private fun parseNonTlvGpoResponse(data: ByteArray): GpoResponse {
    Timber.e("parseNonTlvGpoResponse")
    return GpoResponse(data.copyOfRange(0, 2), data.copyOfRange(2, data.size), byteArrayOf())
}

private fun getGpoCommandDataFromPdol(pDOL: ByteArray): ByteArray {
    val data = mutableListOf<Byte>()
    TLVInputStream(pDOL.inputStream()).use { stream ->
        while (stream.available() > 0) {
            getEmvData(stream.readTag(), stream.readLength()).forEach {
                data.add(it)
            }
        }
    }
    data.add(0, (data.size).toByte())
    data.add(0, 0x83.toByte())

    return data.toByteArray()
}

private fun getEmvData(tag: Int, length: Int): ByteArray {
    val data = when (tag) {
        0x9f_66 -> Hex.hexStringToBytes("A800C000") // TTQ
        0x9f_1a -> Hex.hexStringToBytes("0203") // Country code
        0x5f_2a -> Hex.hexStringToBytes("0203") // Currency code
        0x9a -> Hex.hexStringToBytes(SimpleDateFormat("yyMMdd").format(Date())) // Trn date
        0x9f_5a,
        0x9c -> Hex.hexStringToBytes("00") // trn type
        0x9f_02 -> Hex.hexStringToBytes("000000000001") // amount authorised
        0x9f_35 -> Hex.hexStringToBytes("22") // Terminal type
        0x9f_33 -> Hex.hexStringToBytes("E0A000") // Tcap
        0x9f_40 -> Hex.hexStringToBytes("8e00b05005") // aditional tcap
        0x9f_5c -> Hex.hexStringToBytes("7A45123EE59C7F40") // operator id
        0x9f_37 -> ByteArray(length).apply { SecureRandom().nextBytes(this) } // unpredictable number
        0x9f_58 -> Hex.hexStringToBytes("01") // merchant type indicator
        0x9f_59 -> Hex.hexStringToBytes("C08000") // terminal transaction info
        0x9f_03 -> Hex.hexStringToBytes("000000000000") // amount other
        0x95 -> Hex.hexStringToBytes("0000000000") // tvr
        else -> {
            Timber.e("Unknown gpo required tag: ${Hex.intToHexString(tag)}")
            ByteArray(length) { 0x00 }
        }
    }
    val result = ByteArray(length) { 0x00 }
    result.forEachIndexed { index, _ ->
        if (index < data.size) {
            result[index] = data[index]
        }
    }
    return result
}