package com.rta.demoemvcardreader

import android.nfc.TagLostException
import android.nfc.tech.IsoDep
import net.sf.scuba.tlv.TLVInputStream
import net.sf.scuba.util.Hex
import timber.log.Timber
import java.io.ByteArrayInputStream

fun readCardData(
    isoDep: IsoDep,
    appFileLocator: ByteArray
): CardData? {
    val sfiList = parseAppFileLocator(appFileLocator)
    var pan = ""
    var expiration = ""

    try {
        sfiList.forEach { sfi ->
            Timber.d("-- Sfi: $sfi")
            val responseAPDU = isoDep.transceive(ApduCommands.readSfi(sfi.record, sfi.number))

            if (responseAPDU.isSuccess()) {
                TLVInputStream(responseAPDU.data.inputStream()).use { stream ->
                    stream.readTag() // 70
                    stream.readLength()

                    TLVInputStream(stream.readValue().inputStream()).use { dataStream ->
                        while (dataStream.available() > 0) {
                            val tag = dataStream.readTag()
                            val length = dataStream.readLength()
                            val value = ByteArray(length) { 0x00 }
                            dataStream.read(value, 0, length)

                            if (tag == PAN) {
                                Timber.e("-- PAN: ${Hex.bytesToSpacedHexString(value)}")
                                pan = Hex.bytesToHexString(value)
                            }
                            if (tag == EXPIRATION) {
                                Timber.e("-- Expiration: ${Hex.bytesToSpacedHexString(value)}")
                                expiration = Hex.bytesToHexString(value)
                            }
                        }
                    }
                }
            }
        }
    } catch (exception: TagLostException) {
        return null
    }

    return CardData(pan, expiration)
}

private fun parseAppFileLocator(appFileLocator: ByteArray): List<Sfi> {
    Timber.d("-- Parse AFL: ${Hex.bytesToSpacedHexString(appFileLocator)}")
    if (appFileLocator.size < 4 || appFileLocator.size % 4 != 0) {
        return emptyList()
    }

    val sfiList = mutableListOf<Sfi>()
    val stream = ByteArrayInputStream(appFileLocator)
    val sfiData = ByteArray(4)
    while (stream.available() > 0) {
        stream.read(sfiData, 0, 4)

        Timber.d("-- afl 0: ${sfiData[0]}")
        Timber.d("-- afl 0: ${sfiData[1]}")
        Timber.d("-- afl 0: ${sfiData[2]}")
        Timber.d("-- afl 0: ${sfiData[3]}")


        val number = sfiData[0].toInt().shl(3).shr(3) + 0x04
        for (record in sfiData[1]..sfiData[2]) {
            sfiList.add(Sfi(number, record))
        }
    }

    return sfiList
}
