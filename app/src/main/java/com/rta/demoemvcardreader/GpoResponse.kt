package com.rta.demoemvcardreader

import net.sf.scuba.util.Hex

data class GpoResponse(
    val applicationInterchangeProfile: ByteArray,
    val applicationFileLocator: ByteArray,
    val track2: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GpoResponse

        if (!applicationInterchangeProfile.contentEquals(other.applicationInterchangeProfile)) return false
        if (!applicationFileLocator.contentEquals(other.applicationFileLocator)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = applicationInterchangeProfile.contentHashCode()
        result = 31 * result + applicationFileLocator.contentHashCode()
        return result
    }

    override fun toString(): String =
        "GpoResponse" +
                "\napplicationInterchangeProfile: ${Hex.bytesToSpacedHexString(
                    applicationInterchangeProfile
                )}" +
                "\napplicationFileLocator: ${Hex.bytesToSpacedHexString(applicationFileLocator)}" +
                "\ntrack2: ${Hex.bytesToSpacedHexString(track2)}"
}