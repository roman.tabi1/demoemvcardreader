package com.rta.demoemvcardreader

import android.nfc.TagLostException
import android.nfc.tech.IsoDep
import net.sf.scuba.tlv.TLVInputStream
import net.sf.scuba.util.Hex
import timber.log.Timber
import java.lang.Exception

fun selectAid(isoDep: IsoDep, applicationTemplate: ApplicationTemplate): SelectAidResponse? {

    Timber.d("-- Trying to select application: ${Hex.bytesToPrettyString(applicationTemplate.aid)}")
    val builder = SelectAidResponseBuilder()
    try {
        val responseAPDU =
            isoDep.transceive(ApduCommands.select(applicationTemplate.aid)).also {
                if (!it.isSuccess()) {
                    return null
                }
            }
        try {
            TLVInputStream(responseAPDU.bytes.inputStream()).use { stream ->
                stream.skipToTag(DF_NAME)
                stream.readLength()
                builder.dfName = stream.readValue()

                stream.skipToTag(PDOL)
                stream.readLength()
                builder.pDol = stream.readValue()
            }

        } catch (exception: Exception) {
            Timber.e("Read dfName or pdol error")
        }
        getTagValueAnd(APPLICATION_PREFERRED_NAME, responseAPDU.bytes) {
            builder.appPreferredName = Hex.bytesToASCIIString(it)
        }
        getTagValueAnd(API, responseAPDU.bytes) {
            builder.appPriorityIndicator = Hex.bytesToHexString(it)
        }
        getTagValueAnd(LANG_PREFERENCE, responseAPDU.bytes) {
            builder.languagePreference = Hex.bytesToASCIIString(it)
        }
        getTagValueAnd(ISSUER_CODE_TABLE_INDEX, responseAPDU.bytes) {
            builder.issuerCodeTableIndex = Hex.bytesToHexString(it)
        }
    } catch (exception: TagLostException) {
        return null
    }

    return builder.build()
}

private fun getTagValueAnd(tag: Int, data: ByteArray, withResult: (ByteArray) -> Unit) {
    try {
        TLVInputStream(data.inputStream()).use { stream ->
            stream.skipToTag(tag)
            stream.readLength()
            withResult(stream.readValue())
        }
    } catch (exception: Exception) {
    }
}