package com.rta.demoemvcardreader

import net.sf.scuba.util.Hex

data class SelectAidResponse(val dfName: ByteArray,
                             val pDOL: ByteArray,
                             val logEntry: ByteArray,
                             val appPreferredName: String = "",
                             val appPriorityIndicator: String = "",
                             val languagePreference: String = "",
                             val issuerCodeTableIndex: String = "") {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SelectAidResponse

        if (!dfName.contentEquals(other.dfName)) return false
        if (!pDOL.contentEquals(other.pDOL)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = dfName.contentHashCode()
        result = 31 * result + pDOL.contentHashCode()
        return result
    }

    override fun toString(): String {
        return "SelectAidResponse" +
                "\ndfName: ${Hex.bytesToSpacedHexString(dfName)}" +
                "\npDOL: ${Hex.bytesToSpacedHexString(pDOL)}" +
                "\nlogEntry: ${Hex.bytesToSpacedHexString(logEntry)}" +
                "\nappPreferredName: $appPreferredName" +
                "\nappPriorityIndicator: $appPriorityIndicator" +
                "\nlanguagePreference: $languagePreference" +
                "\nissuerCodeTableIndex: $issuerCodeTableIndex"
    }
}