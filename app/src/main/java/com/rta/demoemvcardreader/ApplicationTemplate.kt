package com.rta.demoemvcardreader

import net.sf.scuba.util.Hex


data class ApplicationTemplate(
    val aid: ByteArray,
    val applicationLabel: ByteArray
) {
    fun isVisa() = Hex.bytesToHexString(aid).startsWith("A000000003")

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ApplicationTemplate

        if (!aid.contentEquals(other.aid)) return false
        if (!applicationLabel.contentEquals(other.applicationLabel)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = aid.contentHashCode()
        result = 31 * result + applicationLabel.contentHashCode()
        return result
    }

    override fun toString(): String {
        return "AppTemplate" +
                "\naid: ${Hex.bytesToSpacedHexString(aid)}" +
                "\nappLabel: ${Hex.bytesToPrettyString(applicationLabel)}"
    }
}