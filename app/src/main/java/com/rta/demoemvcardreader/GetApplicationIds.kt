package com.rta.demoemvcardreader

import android.nfc.TagLostException
import android.nfc.tech.IsoDep
import net.sf.scuba.tlv.TLVInputStream
import net.sf.scuba.util.Hex
import timber.log.Timber
import java.io.ByteArrayInputStream
import java.io.IOException
import java.lang.Exception

private val DF_NAME_CONTACTLESS = "2PAY.SYS.DDF01".toByteArray(Charsets.UTF_8) // Contactless

fun getApplicationIds(isoDep: IsoDep): List<ApplicationTemplate>? {
    return try {
        val list = getApplicationsUsingPpse(isoDep)

        if (list.isEmpty()) {
            Timber.e("-- Read error")
            null
        } else {
            list
        }
    } catch (exception: TagLostException) {
        Timber.e("-- Communication error")
        null
    }
}

private fun getApplicationsUsingPpse(isoDep: IsoDep): List<ApplicationTemplate> {
    Timber.d("-- Trying to get applications using PPSE")

    val responseAPDU = isoDep.transceive(ApduCommands.select(DF_NAME_CONTACTLESS)).also {
        if (!it.isSuccess()) {
            return emptyList()
        }
    }

    val appList = mutableListOf<ApplicationTemplate>()

    try {
        val stream = TLVInputStream(ByteArrayInputStream(responseAPDU.bytes))
        // Search for BF0C - File Control Information (FCI) Issuer Discretionary Data
        stream.skipToTag(FCI)
        val fciLength = stream.readLength()
        val fciValue = stream.readValue()
        Timber.d("-- Tag: BF 0C")
        Timber.d("-- Length: ${Hex.intToHexString(fciLength)}")
        Timber.d("-- Value: ${Hex.bytesToSpacedHexString(fciValue)}")

        stream.close()

        try {
            // Iterate through 61 - application templates
            TLVInputStream(fciValue.inputStream()).use { applicationTemplatesStream ->
                while (applicationTemplatesStream.available() > 2) {
                    with(getApplicationTemplateData(applicationTemplatesStream)) {
                        appList.add(parseApplicationTemplateData(this))
                    }
                }
            }
        } catch (exception: IOException) {
            Timber.e(exception)
            Timber.d("-- No more application templates found")
        }
    } catch (exception: Exception) {
        Timber.e(exception)
        return emptyList()
    }

    return appList
}

private fun getApplicationTemplateData(stream: TLVInputStream): ByteArray =
    with(stream) {
        skipToTag(APP_TEMPLATE)
        readLength()
        readValue()
    }.also { data ->
        Timber.d("-- Application template found: ${Hex.bytesToSpacedHexString(data)}")
    }

private fun parseApplicationTemplateData(source: ByteArray): ApplicationTemplate =
    TLVInputStream(source.inputStream()).use { stream ->
        stream.skipToTag(AID)
        stream.readLength()
        val aid = stream.readValue()
        val appLabel = try {
            stream.skipToTag(APP_LABEL)
            stream.readLength()
            stream.readValue()
        } catch (exception: Exception) {
            Timber.e(
                "-- App label not found in the Application Template data: ${
                    Hex.bytesToSpacedHexString(
                        source
                    )
                }"
            )
            byteArrayOf()
        }

        ApplicationTemplate(aid, appLabel).also {
            Timber.d("-- Application template: $it")
        }
    }